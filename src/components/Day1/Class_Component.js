import React from "react";

export default class ClassComponent extends React.Component {
  //Constructor function for states
  //Will be teaching state and lifecycle within this component
  render() {
    return <div>{"This is a prop " + this.props.property}</div>;
  }
}
