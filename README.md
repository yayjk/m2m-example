<h1>REACT COURSE BREAKDOWN AND SCHEDULE</h1>

**DAY 1**
MODULES COVERED: JSX, DOCUMENT OBJECT MODEL, RENDERING ELEMENTS, COMPONENTS, PROPS, IMPORT AND EXPORT, CONDITIONAL RENDERING

**DAY 2**
MODULES TO COVER: 
- HOW REACT WORKS BEHIND THE SCENES? WEBPACK, BABEL, ES6
- THINKING IN REACT
- ASSIGNMENT FOR DAY 1 CONCEPTS


**DAY 3**
MODULES TO COVER: 
- CLASS COMPONENTS
- STATE AND LIFECYCLE
- HANDLING EVENTS
- ASSIGNMENT ON THE TOPICS COVERED

**DAY 4**
MODULES TO COVER: 
- HOOKS (USESTATE, USEEFFECT)
- NPM
- INCLUDING THIRD PARTY MODULES, 
- REACT-ROUTER
- ASSIGNMENT ON THE TOPICS COVERED


**DAY 5**
MODULES TO COVER: 
- INTRODUCTION TO REDUX, USECONTENT
- ASSIGNMENT EXTENSION OF DAY 4


**DAY 6**
WRAP UP AND TASK ASSIGNMENTS

<h2>**SUMMARY OF TOPICS COVERED:**</h2>

1.  <h3>JSX:</h3>

- JSX is a syntax which enables us to write javascript within html
- [WHY JSX?](https://reactjs.org/docs/introducing-jsx.html#why-jsx) React embraces the fact that rendering logic is inherently coupled with other UI logic: how events are handled, how the state changes over time, and how the data is prepared for display.Instead of artificially separating technologies by putting markup and logic in separate files, React separates concerns with loosely coupled units called “components” that contain both.
- NESTED JSX RULES:
        RULE 1: While writing nested jsx surround the jsx within common parantheses
        RULE 2: NESTED JSX SHOULD ALWAYS HAVE ONE PARENT ELEMENT LIKE `<div> or <> or <React.Fragment>` [reference](https://reactjs.org/docs/react-api.html#reactfragment)
- **ALLOWED:**
```
(<div>
    <p>this is element 1</p>
    <a href="...">link element</a>
</div>)
```
- **NOT ALLOWED**
```
(   
<h1>THIS IS NOT ALLOWED</h1>
<p>BECAUSE h1 and p are siblings without any parent element</p>
)
```
- USE className instead of class and htmlFor instead of for

<hr/>

2.  <h3>[DOM](https://www.w3schools.com/js/js_htmldom.asp)</h3> 

<hr/>

3.  <h3>RENDERING ELEMENTS</h3>

REACTDOM.render(source, destination) 
Explain about applications of react and reactdom library
Explain about render function of the reactdom library along with the parameters

<hr/>

4.  <h3>COMPONENTS</h3>

Explain about the syntactical difference between class components and functional components
Do not explain anything extra. explain only what has been taught so far.

<hr/>

5.  <h3>PROPS</h3>

Props explanation

<hr/>

6.  <h3>IMPORT and EXPORT</h3>

Named exports, default exports, syntax(curly braces and without curly braces). why curly braces is used?

<hr/>

7.  <h3>Conditional Rendering</h3>

if, ternary and &&

<hr/>

> **NOTE**: Provide links if you are refering any docs(compulsary), Please refer the code and try to summarize everything that has been taught yesterday.


                        










