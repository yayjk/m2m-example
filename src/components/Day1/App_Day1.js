import React from "react";
import { FunctionalComponent } from "./Functional_Component";
import ClassComponent from "./Class_Component";

function App() {
  //npx-create-react-app

  //jsx is html but with some extended functionality !!

  //First lets understand why JSX?

  //DOM and virtual dom

  //Extended functionality provided by JSX
  //can use variables within html

  const name = "ozzy";
  const object = "train";

  const jsx_element = <h1>{name}</h1>;

  //JSX can have children
  const nested_jsx = (
    <div>
      {jsx_element}
      <p>{"rides a " + object}</p>
    </div>
  );

  //Somethings to note -> cannot use for and class in jsx
  //here is how it's done
  //<div className="abcd"></div>
  //<label htmlFor="xyz"></div>

  //Rendering elements (react docs)

  // return <div className="App">{nested_jsx}</div>;

  // Functional Component
  return (
    <div className="App">
      <FunctionalComponent property="abcd" />
    </div>
  );

  //Class Component
  // return (
  //   <div className="App">
  //     <ClassComponent property="class property" />
  //   </div>
  // );
}

export default App;
