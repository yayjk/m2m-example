import React from "react";

const ChildComponent = (props) => {
  //JSX ELEMENTS MUST BE WRAPPED IN A HTML TAG LIKE DIV

  //Conditional Rendering
  const numberofTrains = props.numberofTrains;
  let element;
  if (numberofTrains > 1) {
    element = <p>ozzy rides multiple trains</p>;
  } else {
    element = <p>ozzy rides one train</p>;
  }

  return (
    <div>
      <h1>Hello this is the child of FunctionalComponent</h1>
      <p>You can pass props to me</p>
      <p>
        {"I will always render myself acc to the props. For example the prop that's passed to me right now is " +
          props.property}
      </p>
      {element}
      {/* ternary Conditional rendering */}
      {props.booleanProp === true ? (
        <a href="https://www.google.com">Link provided</a>
      ) : (
        <p>Link not available</p>
      )}

      {/* && conditional rendering */}
      {props.anotherProp && <p>An additional prop is avialable</p>}
    </div>
  );
};

export const FunctionalComponent = (props) => {
  //Will be teaching hooks in this component

  return (
    <div>
      <ChildComponent
        property={props.property}
        numberofTrains={1}
        booleanProp={false}
        anotherProp={1}
      />
    </div>
  );
};
