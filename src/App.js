import React, { useState } from "react";
import "./App.css";

function App() {
  const [incoming, setIncoming] = useState();
  const [input2, setInput2] = useState();

  const saveInputToState = (e) => {
    setIncoming(e.target.value);
  };

  return (
    <div className="App">
      <input type="text" onChange={saveInputToState.bind(this)} />
      <input
        type="text"
        onChange={(e) => {
          setInput2(e.target.value);
        }}
      />
      <p>{incoming && incoming}</p>
      <p>{input2 && input2}</p>
    </div>
  );
}

export default App;
