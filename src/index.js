import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

const element = <p>this is how element rendering is done</p>;

ReactDOM.render(<App />, document.getElementById("root"));
